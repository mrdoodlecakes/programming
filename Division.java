/* Print out division of a and b with fractional part included */
public class Division
{
	public static void main(String[] args)
	{
		int a = Integer.parseInt(args[0]);
		int b = Integer.parseInt(args[1]);

		System.out.println(a / b + " and " + a % b + "/" + b);
	}
}