public class RandomNumberProbability
{
	public static final double sampleSize = 100;
	public static void main(String[] args)
	{
		int n = Integer.parseInt(args[0]);
		int[] p = new int[n];

		for(int i = 0; i < sampleSize; i++)
			p[randomNumber(n)]++;;
		for(int i = 1; i <= n; i++)
			System.out.println(i + ": " + (int) ((p[i-1] / sampleSize) * 100) + "%");
	}

	/* Produces random number less than or equal to N */
	public static int randomNumber(int N)
	{
		double r = Math.random();
		int num = (int) (r * N);
		return num;
	}
}