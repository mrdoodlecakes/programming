public class Graph
{
	public static void main(String[] args)
	{
		int N = Integer.parseInt(args[0]);
		String space = "";
		for (int i = 1; i <= N; i++)
		{
			space += " ";	
			System.out.println(i + " " + space + "\\");
		}
	}
}