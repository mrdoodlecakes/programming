public class UseArgument
{
    public static void main(String[] args)
    {
        String name = args[0];
        int age = Integer.parseInt(args[1]);

        System.out.println("This information has been stored.");
        System.out.println("Hello, " + name + ". How does it feel to be " + age + " years old?");
    }
}
